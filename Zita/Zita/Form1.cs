﻿#region Includes
using System;
using System.Text;
using System.Windows.Forms;
using System.Management;
using System.Threading;
using System.IO;

//Myo
using MyoSharp.Device;
using MyoSharp.Communication;
using MyoSharp.Exceptions;
using MyoSharp.Poses;

//Share
using System.Security.AccessControl;
using System.Security.Principal;
using System.Runtime.InteropServices;

//Zoom
using WindowsInput;
using System.Drawing;
#endregion

namespace Zita
{
    public partial class Myo : Form
    {
        #region Variables
        //Myo connection variables
        IChannel myoChannel;
        IHub myoHub;
        NTAccount ntAccount = new NTAccount("Everyone");

        //Start values
        double startRoll = 0.00;
        double startPitch = 0.00;
        double startYaw = 0.00;

        //Current values
        double currentRoll = 0.00;
        double currentPitch = 0.00;
        double currentYaw = 0.00;

        //Zoom state
        int zoomState = 0;

        //App folder
        string appPath;

        int second = 0;

        //Drawing
        [DllImport("User32.dll")]
        static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("User32.dll")]
        static extern int ReleaseDC(IntPtr hwnd, IntPtr dc);
        #endregion

        //Initialization
        public Myo()
        {
            InitializeComponent();
        }

        //Defining starting options
        private void Myo_Load_1(object sender, EventArgs e)
        {
            InitializeMyo();

            //BlockInput(new TimeSpan(0, 1, 0));

            #region Create Zita Folder
            //Get user path
            string userPath = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName;
            if (Environment.OSVersion.Version.Major >= 6)
            {
                userPath = Directory.GetParent(userPath).ToString();
            }

            //Create app folder
            appPath = userPath + "/Zita";
            if (!Directory.Exists(appPath))
            {
                Directory.CreateDirectory(appPath);
            }
            #endregion

            #region Init Zita Folder Share
            // ShareFolderPermission(userPath, "Zita", "Nothing");
            #endregion
        }

        #region Myo Status
        private void InitializeMyo()
        {
            myoChannel = Channel.Create(ChannelDriver.Create(ChannelBridge.Create(), MyoErrorHandlerDriver.Create(MyoErrorHandlerBridge.Create())));
            myoHub = Hub.Create(myoChannel);

            myoHub.MyoConnected += setupMyo;
            myoHub.MyoDisconnected += disconnectMyo;

            myoChannel.StartListening();
        }

        private void disconnectMyo(object sender, MyoEventArgs e)
        {
            MessageBox.Show("Myo Disconnected!");

        }

        private void Myo_FormClosing(object sender, EventArgs e)
        {
            StopMyo();
        }

        private void StopMyo()
        {
            myoChannel.StopListening();
            myoChannel.Dispose();
        }
        #endregion

        #region Define Myo Actions
        private void setupMyo(object sender, MyoEventArgs e)
        {
            richTextBox1.AppendText("Myo Connected!");

            //Zoom
            var fistPose = HeldPose.Create(e.Myo,Pose.Fist);
            fistPose.Interval = TimeSpan.FromSeconds(0.1);
            fistPose.Start();
            fistPose.Triggered += fistDetected;

            //Screenshot
            var spreadPose = HeldPose.Create(e.Myo, Pose.FingersSpread);
            spreadPose.Interval = TimeSpan.FromSeconds(0.1);
            spreadPose.Start();
            spreadPose.Triggered += createScreenshot;

            //PowerPoint
            var nextSlideSeq = PoseSequence.Create(e.Myo, Pose.WaveOut, Pose.WaveIn);
            nextSlideSeq.PoseSequenceCompleted += NextSlideSeq_PoseSequenceCompleted; ;

            e.Myo.OrientationDataAcquired += Myo_OrientationDataAcquired;
        }
        #endregion

        #region PowerPoint
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        private string GetActiveWindowTitle()
        {
            const int nChars = 256;
            StringBuilder Buff = new StringBuilder(nChars);
            IntPtr handle = GetForegroundWindow();

            if (GetWindowText(handle, Buff, nChars) > 0)
            {
                return Buff.ToString();
            }
            return null;
        }

        private void NextSlideSeq_PoseSequenceCompleted(object sender, PoseSequenceEventArgs e)
        {
 //           if (GetActiveWindowTitle().Contains("PowerPoint"))
//            {
                InputSimulator.SimulateKeyPress(VirtualKeyCode.RIGHT);
      //      }
        }

        #endregion

        #region Zoom
        private void fistDetected(object sender, PoseEventArgs e)
        {

            if (currentRoll < (startPitch + 5) && zoomState <= 2) {
                InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.LWIN, VirtualKeyCode.OEM_PLUS);
                InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.LWIN, VirtualKeyCode.OEM_PLUS);
                zoomState += 1;
                Console.WriteLine("kkk");
            } else if (currentRoll > (startPitch + 5)) {
                InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.LWIN, VirtualKeyCode.OEM_MINUS);
                zoomState -= 1;
                Console.WriteLine("kkk");
                AutoClosingMessageBox.Show("", "", 0);
                InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.MENU, VirtualKeyCode.F4);
//                InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.MENU, VirtualKeyCode.F4);
            }
        }

        public class AutoClosingMessageBox
        {
            System.Threading.Timer _timeoutTimer;
            string _caption;
            AutoClosingMessageBox(string text, string caption, int timeout)
            {
                _caption = caption;
                _timeoutTimer = new System.Threading.Timer(OnTimerElapsed,
                    null, timeout, System.Threading.Timeout.Infinite);
                using (_timeoutTimer)
                    MessageBox.Show(text, caption);
            }
            public static void Show(string text, string caption, int timeout)
            {
                new AutoClosingMessageBox(text, caption, timeout);
            }
            void OnTimerElapsed(object state)
            {
                IntPtr mbWnd = FindWindow("#32770", _caption); // lpClassName is #32770 for MessageBox
                if (mbWnd != IntPtr.Zero)
                    SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
                _timeoutTimer.Dispose();
            }
            const int WM_CLOSE = 0x0010;
            [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
            static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
            [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
            static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
        }
        #endregion

        #region Create Screenshot
        private void createScreenshot(object sender, PoseEventArgs e)
        {
            saveScreenshot();
        }

        public void saveScreenshot() {
            DateTime currentTime = DateTime.Today;
            SendKeys.SendWait("{PRTSC}");
            Image img = Clipboard.GetImage();
            string imageName = appPath + "/drawing" + currentTime.ToFileTime() + ".jpg";
            img.Save(imageName, System.Drawing.Imaging.ImageFormat.Jpeg);
            MessageBox.Show("dasdsa");
        }
        #endregion

        #region Pose and Orientation
        private void Myo_OrientationDataAcquired(object sender, OrientationDataEventArgs e)
        {
            const float PI = (float)System.Math.PI;

            currentRoll = (e.Roll + PI) / (PI * 2.0f) * 100;
            currentPitch = (e.Pitch + PI) / (PI * 2.0f) * 100;
            currentYaw = (e.Yaw + PI) / (PI * 2.0f) * 100;

            if (startPitch == 0.00) {
                startRoll = currentRoll;
                startPitch = currentPitch;
                startYaw = currentYaw;
            }

            string data = "Roll: " + currentRoll.ToString("#.##") + Environment.NewLine + "Pitch:" + currentPitch.ToString("#.##") + Environment.NewLine + "Yaw:" + currentYaw.ToString("#.##") + Environment.NewLine;
            
            InvokeData(data);
        }

        #endregion

        #region Display Myo data
        private void InvokeData(string data) {
            if (InvokeRequired) {
                this.Invoke(new Action<string>(InvokeData), new object[] { data });
                return;
            }
            richTextBox1.Clear();
            richTextBox1.AppendText(data + Environment.NewLine);
        }
        #endregion

        #region Block Keyboard and Mouse

        public partial class NativeMethods
        {

            /// Return Type: BOOL->int
            ///fBlockIt: BOOL->int
            [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "BlockInput")]
            [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
            public static extern bool BlockInput([System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)] bool fBlockIt);

        }

        public static void BlockInput(TimeSpan span)
        {
            try
            {
                NativeMethods.BlockInput(true);
                Thread.Sleep(span);
            }
            finally
            {
                NativeMethods.BlockInput(false);
            }
        }

        #endregion

        #region Share Folder
        public void ShareFolderPermission(string FolderPath, string ShareName, string Description)
        {
            try
            {
                SecurityIdentifier oGrpSID = (SecurityIdentifier)ntAccount.Translate(typeof(SecurityIdentifier));
                byte[] utenteSIDArray = new byte[oGrpSID.BinaryLength];
                oGrpSID.GetBinaryForm(utenteSIDArray, 0);
                ManagementObject oGrpTrustee = new ManagementClass(new ManagementPath("Win32_Trustee"), null);
                oGrpTrustee["Name"] = "Everyone";
                oGrpTrustee["SID"] = utenteSIDArray;
                ManagementObject oGrpACE = new ManagementClass(new ManagementPath("Win32_Ace"), null);
                oGrpACE["AccessMask"] = 2032127;//Full access
                oGrpACE["AceFlags"] = AceFlags.ObjectInherit | AceFlags.ContainerInherit; //propagate the AccessMask to the subfolders
                oGrpACE["AceType"] = AceType.AccessAllowed;
                oGrpACE["Trustee"] = oGrpTrustee;
                ManagementObject oGrpSecurityDescriptor = new ManagementClass(new ManagementPath("Win32_SecurityDescriptor"), null);
                oGrpSecurityDescriptor["ControlFlags"] = 4; //SE_DACL_PRESENT
                oGrpSecurityDescriptor["DACL"] = new object[] { oGrpACE };

                ManagementClass mc = new ManagementClass("win32_share");
                ManagementBaseObject inParams = mc.GetMethodParameters("Create");
                inParams["Description"] = Description;
                inParams["Name"] = ShareName;
                inParams["Path"] = FolderPath;
                inParams["Type"] = 0x0; //Disk Drive
                inParams["MaximumAllowed"] = null;
                inParams["Password"] = null;
                inParams["Access"] = oGrpSecurityDescriptor;
                ManagementBaseObject outParams = mc.InvokeMethod("Create", inParams, null);

                if ((uint)(outParams.Properties["ReturnValue"].Value) != 0)
                    MessageBox.Show("Folder might be already in share or unable to share the directory");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

    }
}
